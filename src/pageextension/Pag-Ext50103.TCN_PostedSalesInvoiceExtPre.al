pageextension 50103 "TCN_PostedSalesInvoiceExtPre" extends "Posted Sales Invoice"  //MyTargetPageId
{
    layout
    {

    }

    actions
    {
        addfirst(Navigation)
        {
            action(PruebaImpresion)
            {
                Image = AboutNav;
                Caption = 'Prueba Impresion Factura';

                trigger OnAction()
                var
                    rlSalesInvoiceHeader: Record "Sales Invoice Header";
                    rlSalesInvoiceHeaderTMP: Record "Sales Invoice Header" temporary;
                    xlFiltro: Text;

                begin
                    // Solucion 01   Mas optima
                    rlSalesInvoiceHeader := Rec; // copia y pasa todos los datos. Rec= registro asociado a la pagina
                    rlSalesInvoiceHeader.Find(); // busca el registro de los campos de la clave
                    rlSalesInvoiceHeader.SetRecFilter(); //el filtro para que me devuelva ese registro
                    report.Run(Report::"Sales - Invoice", true, false, rlSalesInvoiceHeader);    // lo lanzamos con el filtro

                    // Solucion 02
                    // xlFiltro := GetView();    //por defecto es true, con la cadena de los campos
                    // Message(xlFiltro);  //veo que me esta guardando
                    // SetRecFilter(); //pone un filtro unico, para ese exclusivo registro
                    // report.run(report::"Sales - Invoice", true, false, Rec);
                    // Reset();
                    // SetView(xlFiltro);

                    // Solucion03    guardo los filtros en la tabla temporal
                    // rlSalesInvoiceHeaderTMP.CopyFilters(Rec);
                    // SetRecFilter(); //pone un filtro unico, para ese exclusivo registro
                    // report.run(report::"Sales - Invoice", true, false, Rec);
                    // Reset();
                    // CopyFilters(rlSalesInvoiceHeaderTMP);

                end;
            }
        }
    }
}