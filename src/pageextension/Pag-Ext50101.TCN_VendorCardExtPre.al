pageextension 50101 "TCN_VendorCardExtPre" extends "Vendor Card" //MyTargetPageId
{
    layout //campos
    {
        modify("Balance (LCY)")
        {
            Visible = false;
        }
    }

    actions //botones
    {

    }

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    begin
        if not Confirm('¿Desea salir de la pagina?', true) then begin
            exit(false);
        end;
    end;
}