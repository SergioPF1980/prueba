pageextension 50102 "TCN_CustomerListExtPru" extends "Customer List" //MyTargetPageId
{   //OJO
    layout
    {
        modify("Balance (LCY)")
        {
            Visible = xMostrarCalculados;
        }
        modify("Balance Due (LCY)")
        {
            Visible = xMostrarCalculados;
        }
        modify("Sales (LCY)")
        {
            Visible = xMostrarCalculados;
        }
        modify("Payments (LCY)")
        {
            Visible = xMostrarCalculados;
        }
        addlast(Control1)
        {
            field(ImportePedidoFacturas; cuTCN_RegistrosVarios.ImportePedidosFacturasClienteF(Rec, false))
            {
                Caption = 'Importe Pedido + Facturas';
                trigger OnDrillDown()
                begin
                    cuTCN_RegistrosVarios.ImportePedidosFacturasClienteF(rec, true);
                end;
            }
        }
    }
    actions
    {
        addlast(Creation)
        {
            action(GetFilterNo) //nos da todos los campos
            {
                Caption = 'Ejemplo GetFilter campo Nº';
                trigger OnAction()
                var

                begin
                    Message(GetFilters());
                end;
            }
            action(GetFiltersNo)
            {
                Caption = 'Ejemplo GetFilters campo Nº';
                trigger OnAction()
                var

                begin
                    Message(GetFilter("No."));
                end;
            }

        }
        addfirst(Reporting)
        {
            action(Ocuntar)
            {
                Caption = 'Ocuntar Campos Calculados';
                Image = BreakRulesOff;
                Visible = xMostrarCalculados;

                trigger OnAction()
                begin
                    xMostrarCalculados := false;
                end;
            }
            action(Mostrar)
            {
                Caption = 'Mostrar Campos Calculados';
                Image = BreakRulesOn;
                Visible = not xMostrarCalculados;

                trigger OnAction()
                begin
                    xMostrarCalculados := true;
                end;
            }
        }
    }
    var
        cuTCN_RegistrosVarios: Codeunit TCN_RegistrosVarios;
        xMostrarCalculados: Boolean;
}