pageextension 50104 "TCN_CustomerLookupExtPru" extends "Customer Lookup" //MyTargetPageId
{
    layout
    {
        // No lo oculta porque la ultima capa q monta es la q predomina
        modify("Credit Limit (LCY)") // oculta campo
        {
            Visible = xCreditoMaximoVisible;
        }
        modify("Phone No.") // oculta campo
        {
            Visible = xCreditoMaximoVisible;
        }
    }
    procedure GetSelectionFilterF(var prCustomer: Record Customer)
    begin
        // es de pagina
        SetSelectionFilter(prCustomer);   //devuelve el parametro del filtro

    end;
    //  OJO
    procedure VisibilidadCreditoMaximoF(pVisible: Boolean)
    var
    begin
        xCreditoMaximoVisible := pVisible;

    end;

    var
        xCreditoMaximoVisible: Boolean;
}