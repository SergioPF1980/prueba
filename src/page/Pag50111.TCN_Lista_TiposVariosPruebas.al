page 50111 "TCN_Lista_TiposVariosPruebas"
{
    DataCaptionExpression = xDescLista; // variable o una función
    PageType = List;
    SourceTable = TCN_TiposVariosPruebas;
    Caption = 'Lista Tipos Varios Pruebas';
    ApplicationArea = All;
    UsageCategory = Lists;

    ShowFilter = false; //muestra 


    layout
    {
        area(content)
        {
            repeater(General)
            {
                // field(Tipo; Tipo)
                // {
                //     ApplicationArea = All;
                //     visible = false;
                //     Enabled = false;
                // }
                field(Codigo; Codigo)
                {
                    //OJO
                    CaptionClass = StrSubstNo('3, Código %1', Tipo);
                    ApplicationArea = All;
                }
                field(Descripcion; Descripcion)
                {
                    ApplicationArea = All;
                }
                field(Palmeada; Palmeada)
                {
                    ApplicationArea = All;
                    Visible = xPalmeadaVisible;
                }

            }
        }
    }

    var
        xDescLista: Text;
        xPalmeadaVisible: Boolean;

    local procedure AsignarCamposVisiblesF()
    var
        xlGrupoFiltroInicial: Integer;
        i: Integer;
    begin
        xlGrupoFiltroInicial := FilterGroup;
        for i := 0 to 10 do begin   //recorremos todos los grupos

            FilterGroup(i);
            if GetFilter(tipo) <> '' then begin
                xDescLista := StrSubstNo('Lista %1s', GetFilter(Tipo));
            end;
            if GetFilter(Tipo) = Format(tipo::pata) then begin
                xPalmeadaVisible := true;
                i := 11;
            end;
        end;
        FilterGroup(xlGrupoFiltroInicial);
    end;

    trigger OnOpenPage()
    begin
        AsignarCamposVisiblesF();
    end;


}
