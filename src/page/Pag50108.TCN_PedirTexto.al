page 50108 "TCN_PedirTexto"
{
    Caption = 'Pedir Texto';
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = Integer;  //una pag. necesita siempre una tabla. Tiene que llevar una condicion
    SourceTableView = where (Number = const (0));   //elimina anterior/siguiente

    layout
    {
        area(Content)
        {
            group(GroupName)
            {
                field(TextoAIntroducir; xTexto)
                {
                    Caption = 'TextoAIntroducir';
                    ApplicationArea = All;

                }
            }
        }
    }
    var
        xTexto: Text;

}