page 50106 "TCN_ListaLineas"
{
    Caption = 'Lista Lineas';
    PageType = ListPart;    //guarda todas las lineas
    // UsageCategory = Lists;
    // ApplicationArea = All;
    SourceTable = TCN_Lines;
    AutoSplitKey = true;    //guarda automaticamente las lineas
    PopulateAllFields = true;   //filtro con nuevo valor me da el numero de linea automaticamente

    layout
    {
        area(Content)
        {
            repeater(Group)
            {
                field(Codigo; Codigo)
                {
                    ApplicationArea = All;
                }
                field(DescVenta; DescVenta)
                {
                    ApplicationArea = All;
                }
                field(FechaVenta; FechaVenta)
                {
                    ApplicationArea = All;
                }
                field(NumLinea; NumLinea)
                {
                    ApplicationArea = All;
                }

            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(ActionName)
            {
                ApplicationArea = All;

                trigger OnAction();
                begin

                end;
            }
        }
    }
}