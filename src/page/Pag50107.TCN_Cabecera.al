page 50107 "TCN_Cabecera"
{
    Caption = 'Venta Clientes';
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Documents;
    SourceTable = TCN_Cabecera;

    layout
    {
        area(Content)
        {
            group(General)
            {
                field(Codigo; Codigo)
                {
                    ApplicationArea = All;
                }
                field(Descripcion; Descripcion) // Campo de la página
                {
                    Visible = Codigo <> '';
                    ApplicationArea = All;
                }
            }

            group(Informacion)
            {
                field(DescripcionInf; Descripcion) // dos objetos con nombre diferente que apuntan al mismo de la tabla
                {

                }
            }
            part(Lineas; TCN_ListaLineas)
            {
                SubPageLink = Codigo = field (Codigo);  //Enlazamos con la lista. Pnemos el codigo
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(Mensaje)
            {
                ApplicationArea = All;

                trigger OnAction()
                begin
                    Message('Hola Clase');
                end;
            }
        }
        area(Navigation)
        {
            action(InfEmpresa)
            {
                Caption = 'Información Empresa';
                Image = AddContacts;
                RunObject = page "Company Information";
            }
            action(ClientesVarios)
            {
                Caption = 'Lista Clientes Varios';
                Image = Customer;
                RunObject = page "Customer list";
                RunPageLink = "Country/Region Code" = filter ('*ES*');
            }
            action(PedirDatos)
            {
                Caption = 'Pedir Datos';
                Image = Customer;
                RunObject = page TCN_PedirTexto;
            }
            action(CreditoPorGrupoCliente)
            {
                Caption = 'Credito Por Grupo Cliente';
                Image = Action;
                trigger OnAction()
                var
                    rlCustomer: Record Customer;
                    rlCustomerTMP: Record Customer temporary;
                begin
                    rlCustomer.FindSet(false);
                    repeat
                        rlCustomerTMP.SetRange("Customer Posting Group", rlCustomer."Customer Posting Group");
                        if not rlCustomerTMP.FindFirst() then begin
                            rlCustomerTMP.Init();
                            rlCustomerTMP."No." := rlCustomer."Customer Posting Group";
                            rlCustomerTMP."Customer Posting Group" := rlCustomer."Customer Posting Group";
                            rlCustomerTMP.Insert(false);
                        end;
                        rlCustomerTMP."Credit Limit (LCY)" += rlCustomer."Credit Limit (LCY)";
                        rlCustomerTMP.Modify(false);

                    until rlCustomer.Next() = 0;
                end;
            }
        }
    }

    var
        myInt: Integer;
}