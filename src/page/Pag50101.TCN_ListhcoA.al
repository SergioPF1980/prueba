page 50101 "TCN_ListhcoA"
{
    Caption = 'Lista Historico A';
    PageType = List;
    ApplicationArea = All;
    UsageCategory = Lists;
    SourceTable = TCN_HCOA;

    layout
    {
        area(Content)
        {
            group(GroupNameG)
            {
                field(Color; Color)
                {
                    ApplicationArea = All;
                }
                field(DiaSemana; DiaSemana)
                {
                    ApplicationArea = All;
                }
                field(FechaRegistro; FechaRegistro)
                {
                    ApplicationArea = All;
                }
                field(Talla; Talla)
                {
                    ApplicationArea = All;
                }

            }
        }
    }

    actions
    {
        area(Creation)
        {
            action(SetValor)
            {
                trigger OnAction()

                begin
                    cuRegistrosVarios.SetValorF('Hola Curso');
                end;
            }
            action(GetValor)
            {
                trigger OnAction()

                begin
                    Message(cuRegistrosVarios.GetValorF());
                end;
            }
        }
    }
    var
        curegistrosVarios: Codeunit TCN_registrosVarios;
}