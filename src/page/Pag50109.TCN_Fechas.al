page 50109 "TCN_Fechas"
{
    Caption = 'TCN_Fechas';
    PageType = List;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = Date;

    layout
    {
        area(Content)
        {
            repeater(GroupName)
            {

                field("Period End"; "Period End")
                {
                    ApplicationArea = All;
                }
                field("Period Name"; "Period Name")
                {
                    ApplicationArea = All;
                }
                field("Period No."; "Period No.")
                {
                    ApplicationArea = All;
                }
                field("Period Start"; "Period Start")
                {
                    ApplicationArea = All;
                }
                field("Period Type"; "Period Type")
                {
                    ApplicationArea = All;
                }
            }
        }
    }

}