page 50103 "TCN_FichaAves"
{

    PageType = Card;
    SourceTable = TCN_Ave;
    Caption = 'Ficha Aves';


    layout
    {
        area(content)
        {
            group(General)
            {
                field(Codigo; Codigo)
                {
                    ApplicationArea = All;
                }
            }
            group(Otros)
            {
                field(Nombre; Nombre)
                {
                    ApplicationArea = All;
                }
            }
        }
    }

}
