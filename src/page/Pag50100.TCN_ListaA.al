page 50100 "TCN_ListaA"
{

    PageType = List;
    SourceTable = TCN_A;
    Caption = 'ListaA';
    ApplicationArea = All;
    UsageCategory = Lists;


    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Color; Color)
                {
                    ApplicationArea = All;
                }
                field(diaSemana; diaSemana)
                {
                    ApplicationArea = All;
                }
                field(Talla; Talla)
                {
                    ApplicationArea = All;
                }
                field(Descripcion; Descripcion)
                {
                    ApplicationArea = All;
                }
                field(NumCliente; NumCliente)
                {
                    ApplicationArea = All;
                }
                field(NombreCliente; NombreCliente)
                {
                    ApplicationArea = All;
                }

                field(NombreClienteFuncion; cuRegistrosVarios.NombreClienteF(NumCliente))
                {
                    Caption = 'Nombre Cliente por Funcion';
                }
                field(SumaImporte; SumaImporte)
                {
                    ApplicationArea = All;
                }
                field(NumPersona; NumPersona)
                {
                    ApplicationArea = All;
                }
                field(TipoPersona; TipoPersona)
                {
                    ApplicationArea = All;
                }
                field(CodAve; CodAve)
                {
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        area(Creation)
        {
            action(SetValor)
            {
                trigger OnAction()

                begin
                    cuRegistrosVarios.SetValorF('Hola Curso');
                end;
            }
            action(GetValor)
            {
                trigger OnAction()

                begin
                    Message(cuRegistrosVarios.GetValorF());
                end;
            }
            action(EjecutarCodeUnit)
            {
                trigger OnAction()
                var
                    rlAveTMP: Record TCN_Ave temporary;
                    culRegistrosVarios: Codeunit TCN_RegistrosVarios;
                    i: Integer;
                begin
                    //rlAveTMP.DeleteAll(false);
                    for i := 1 to 10 do begin
                        // Commit(); 
                        if not culRegistrosVarios.Run() then begin
                            rlAveTMP.Init();
                            rlAveTMP.Codigo := Format(i); //format pasa a texto
                            rlAveTMP.Nombre := GetLastErrorText;   //sale error del ultimo escrito
                            rlAveTMP.Insert(true);
                        end;
                    end;
                    //muestra los valores
                    page.Run(0, rlAveTMP);
                end;
            }

            action(SobreCarga)
            {
                trigger OnAction()
                var
                    culRegistroVarios: Codeunit TCN_RegistrosVarios;
                    xlBigInteger: BigInteger;
                    xlFecha: Date;
                    xlDateFormula: Text;
                begin
                    xlBigInteger := 999999;
                    xlFecha := today;
                    xlDateFormula := '1S';

                    Message(culRegistroVarios.SobrecargaF(xlBigInteger));
                    Message(culRegistroVarios.SobrecargaF(xlFecha));
                    Message(culRegistroVarios.SobrecargaF(xlFecha, xlDateFormula));
                end;
            }

            action(SegundoPlano)
            {
                trigger OnAction()
                var
                    culRegistroVarios: Codeunit TCN_RegistrosVarios;
                    xlNumSesionIniciada: Integer;
                    xlSesionIniciada: Boolean;
                begin
                    culRegistroVarios.EjecucionSegundoPlanoF();
                    // xlSesionIniciada := StartSession(xlNumSesionIniciada, Codeunit::TCN_RegistrosVarios);
                    // Message(Format(xlNumSesionIniciada));
                end;
            }


            action(PruebasVarias)
            {
                trigger OnAction()
                begin
                    cuRegistrosVarios.Run();
                end;
            }
        }
    }
    var
        cuRegistrosVarios: Codeunit TCN_registrosVarios;


}
