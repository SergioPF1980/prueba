page 50104 "TCN_DocA"
{
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = TCN_A;

    layout
    {
        area(Content)
        {
            group(GroupName)
            {
                field(Name; CodAve)
                {
                    ApplicationArea = All;

                }
            }
            part(Lineas; TCN_Lista02)
            {

            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(ActionName)
            {
                ApplicationArea = All;

                trigger OnAction()
                begin

                end;
            }
        }
    }

    var
        myInt: Integer;
}