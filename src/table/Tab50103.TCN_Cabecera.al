table 50103 "TCN_Cabecera"
{
    DataClassification = ToBeClassified;

    fields
    {
        field(1; "Codigo"; Code[10])
        {
            Caption = 'Código';
            DataClassification = ToBeClassified;
        }
        field(2; "Descripcion"; Text[20])
        {
            Caption = 'Descripción';
        }

    }

    keys
    {
        key(PK; "Codigo")
        {
            Clustered = true;
        }
    }

}