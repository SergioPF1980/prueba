table 50100 "TCN_A"
{
    DataClassification = ToBeClassified;
    Caption = 'Tabla A';
    LookupPageId = TCN_ListaA;
    DrillDownPageId = TCN_ListaA;


    fields
    {
        field(1; Color; Enum TCN_Colores)
        {
            Caption = 'Color';
            DataClassification = ToBeClassified;

        }
        field(2; Talla; Enum TCN_Tallas)
        {

        }
        field(3; diaSemana; Option)
        {
            Caption = 'Día Semana';
            OptionMembers = Lunes,Martes,Miercoles,Jueves;

        }
        field(4; NumCliente; Code[20])
        {
            Caption = 'Nº Cliente';
            TableRelation = Customer."No.";
            //TableRelation = Customer."No." where ("Phone No." = filter (<> ''));
            trigger OnValidate()
            var
                myInt: Integer;
            begin
                TestField(Talla, Talla::XL);
                Message('Has elegido el Cliente %1', NumCliente);
            end;
        }
        field(5; Descripcion; Text[250])
        {
            trigger OnValidate()
            begin
                TestField(Talla);   //si no le indicas la talla no te deja escribir
                Descripcion := UpperCase(Descripcion);
            end;
        }
        field(6; TipoPersona; enum TCN_TipoPersona)
        {
            Caption = 'Cliente o Proveedor';
            InitValue = 'Cliente';
        }

        field(7; NumPersona; Code[20])
        {
            Caption = 'Nº Cte/Prov';
            TableRelation = if (TipoPersona = const (Cliente)) Customer."No." else
            Vendor."No.";
            trigger OnValidate()
            begin
                if not (TipoPersona in [TipoPersona::Cliente, TipoPersona::Proveedor]) then begin
                    FieldError(TipoPersona, 'Debe ser Cliente o Proveedor');
                end;
            end;

            trigger OnLookup()
            var
                plCustomerLookup: Page "Customer Lookup";
                plVendorLookup: Page "Vendor Lookup";
                rlCustomer: Record Customer;
                rlVendor: Record Vendor;
            begin
                case TipoPersona of
                    tipopersona::Cliente:
                        with rlCustomer do begin
                            if get(rec.NumPersona) then begin

                            end;
                            // Te posicionas en el cliente que seleccionas
                            //plCustomerLookup.SetTableView(rlCustomer); //no hace falta
                            plCustomerLookup.SetRecord(rlCustomer);
                            plCustomerLookup.LookupMode(true);
                            if plCustomerLookup.RunModal() in [Action::LookupOK, Action::OK] then begin
                                plCustomerLookup.GetRecord(rlCustomer);
                                rec.Validate(NumPersona, "No.");
                            end;
                        end;
                    TipoPersona::Proveedor:
                        with rlVendor do begin
                            if get(rec.NumPersona) then begin

                            end;
                            // Te posicionas en el cliente que seleccionas
                            //plCustomerLookup.SetTableView(rlCustomer); //no hace falta
                            plVendorLookup.SetRecord(rlVendor);
                            plVendorLookup.LookupMode(true);
                            plCustomerLookup.VisibilidadCreditoMaximoF(false);
                            if plVendorLookup.RunModal() in [Action::LookupOK, Action::OK] then begin
                                plVendorLookup.GetRecord(rlVendor);
                                rec.Validate(NumPersona, "No.");
                            end;
                        end;
                end;
            end;
        }
        field(8; SumaImporte; Decimal)
        {
            FieldClass = FlowField;
            CalcFormula = sum ("Sales Line".Amount
                                where ("Sell-to Customer No." = field (NumCliente),
                                        "Shipment Date" = field (filtroFecha),
                                        "No." = field (FiltroProducto)
                                )
                            );
        }
        field(9; filtroFecha; Date)
        {
            Caption = 'Filtro Fecha';
            FieldClass = FlowFilter; //no se crea formula. Es para traer el campo para filtrar
        }

        field(10; FiltroProducto; Code[20])
        {
            Caption = 'Filtro Producto';
            FieldClass = FlowFilter;
            TableRelation = item."No.";
        }

        field(11; NombreCliente; Text[2048])
        {
            Caption = 'Nombre Cliente';
            FieldClass = FlowField;
            CalcFormula = lookup (Customer.Name
                                where ("No." = field (NumCliente))
                            );
        }
        field(12; CodAve; Code[20])
        {
            Caption = 'Código Ave';
            TableRelation = TCN_Ave.Codigo;

        }

    }

    keys
    {
        key(PK; Color)
        {
            Clustered = true;
        }
        key(Secundaria; NumCliente, Color, diaSemana)
        {

        }
    }

    var
        myInt: Integer;

    trigger OnInsert()
    begin
        Message('Registro Creado');

    end;

    trigger OnModify()
    begin

    end;

    trigger OnDelete()
    begin

    end;

    trigger OnRename()
    begin
        Error('No se permite cambio de clave');
    end;

}