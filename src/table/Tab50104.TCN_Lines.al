table 50104 "TCN_Lines"
{
    DataClassification = ToBeClassified;

    fields
    {
        field(1; "Codigo"; Code[10])
        {
            Caption = 'Código';
            DataClassification = ToBeClassified;
            TableRelation = TCN_Lines.Codigo;
        }
        field(2; "NumLinea"; Integer)
        {
            Caption = 'Nº Línea';
            DataClassification = ToBeClassified;
        }
        field(3; "FechaVenta"; Date)
        {
            Caption = 'Fecha de Venta';
            DataClassification = ToBeClassified;
        }
        field(4; "DescVenta"; Text[250])
        {
            Caption = 'Descripción Venta';
            DataClassification = ToBeClassified;
        }
    }

    keys
    {
        key(PK; Codigo, NumLinea) //Codigo+numero de linea
        {
            Clustered = true;
        }
    }

    local procedure VerificarDatos()
    var
        xlTexto: Text;
    begin
        OnBeforeVerificarDatosLineasF();

        //
        //
        //

        OnAfterVerificarDatosLineasF(Rec, xRec);
    end;

    [IntegrationEvent(false, true)]
    local procedure OnBeforeVerificarDatosLineasF()
    begin

    end;

    [IntegrationEvent(false, true)]
    local procedure OnAfterVerificarDatosLineasF(var RecLineas: Record TCN_Lines; xRecLineas: Record TCN_Lines)
    begin

    end;

    trigger OnInsert()
    begin
        VerificarDatos();
    end;

    var
        //Variable
        xTextoGlobal: Text;
        xVariable: array[10, 10] of Text;


        //Constante
        cMsgError: Label 'Proceso Cancelado';

}