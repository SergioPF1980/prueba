table 50101 "TCN_HCOA"
{
    DataClassification = ToBeClassified;

    fields
    {
        field(1; Color; Enum TCN_Colores)
        {
            Caption = 'Color';
            DataClassification = ToBeClassified;

        }
        field(2; Talla; Enum TCN_Tallas)
        {

        }
        field(3; DiaSemana; Option)
        {
            OptionMembers = Lunes,Martes,Miercoles;

        }
        field(4; FechaRegistro; Date)
        {
            Caption = 'Fecha Registro';
            DataClassification = ToBeClassified;
        }
    }

    keys
    {
        key(PK; Color)
        {
            Clustered = true;
        }
    }

    var
        myInt: Integer;

    trigger OnInsert()
    begin
        Message('Registro Creado');
    end;

    trigger OnModify()
    begin

    end;

    trigger OnDelete()
    begin

    end;

    trigger OnRename()
    begin
        Error('No se permite cambio de clave');
    end;

}