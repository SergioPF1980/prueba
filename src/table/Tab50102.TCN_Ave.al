table 50102 "TCN_Ave"
{
    DataClassification = ToBeClassified;
    LookupPageId = TCN_ListaAves;
    DrillDownPageId = TCN_ListaAves;    //aparece nuevo y el nombre en avanzado

    fields
    {
        field(1; "Codigo"; Code[10])
        {
            Caption = 'Código';
            DataClassification = ToBeClassified;
        }
        field(2; Nombre; Text[50])
        {
            trigger OnValidate()
            var
                rlCustomer: Record Customer;
            begin
                Message('Has escrito %1 en el nombre del registro con código %2', Nombre, Codigo);
                rlCustomer.Init();
                rlCustomer."No." := Codigo;
                rlCustomer.Insert();
            end;
        }
        field(3; Preguntar; Boolean)
        {
            trigger OnValidate()
            var
                culTCN_SuscripcionesVar: Codeunit TCN_SuscripcionesVar;
            begin
                if Preguntar then begin
                    BindSubscription(culTCN_SuscripcionesVar);
                end else begin
                    UnbindSubscription(culTCN_SuscripcionesVar);
                end;
            end;
        }
        field(4; TipoPico; Code[10])
        {
            Caption = 'Tipo Pico';
            TableRelation = TCN_TiposVariosPruebas.Codigo where (Tipo = const (Pico));
        }
        field(5; TipoPluma; Code[10])
        {
            Caption = 'Tipo Pluma';
            TableRelation = TCN_TiposVariosPruebas.Codigo where (Tipo = const (Pluma));
        }
        field(6; TipoPata; Code[10])
        {
            Caption = 'Tipo Pata';
            TableRelation = TCN_TiposVariosPruebas.Codigo where (Tipo = const (Pata));
        }
    }

    keys
    {
        key(PK; "Codigo")
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
        fieldgroup(DropDown; Codigo, Nombre)
        {

        }

    }

}