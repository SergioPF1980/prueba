table 50105 "TCN_TiposVariosPruebas"
{
    DataClassification = ToBeClassified;
    LookupPageId = TCN_Lista_TiposVariosPruebas;

    fields
    {
        field(1; "Tipo"; Enum TCN_TipoCampo)
        {
            Caption = 'Tipo';
            DataClassification = ToBeClassified;
        }

        field(2; "Codigo"; code[10])
        {
            Caption = 'Código';
            DataClassification = ToBeClassified;
        }
        field(3; "Descripcion"; Text[100])
        {
            Caption = 'Descripción';
            DataClassification = ToBeClassified;
        }
        field(4; "Palmeada"; Boolean)
        {
            Caption = 'Palmeada';
            DataClassification = ToBeClassified;
        }

    }

    keys
    {
        key(PK; "Tipo", Codigo)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
        fieldgroup(DrowDown; Codigo, Descripcion)
        {

        }
    }

}