tableextension 50100 "TCN_ItemExtPru" extends Item //MyTargetTableId
{
    fields
    {
        field(50100; CodAlmacen2; Code[20])
        {
            Caption = 'Cód Almacén 2';
            TableRelation = Location.Code;
        }

        modify(Description)
        {
            trigger OnAfterValidate()
            begin
                Description := LowerCase(Description);
            end;
        }
    }

}