codeunit 50101 "TCN_SuscripcionesVar"
{
    SingleInstance = true;
    // EventSubscriberInstance = Manual; Se activa por defecto

    [EventSubscriber(ObjectType::Table, Database::customer, 'OnAfterInsertEvent', '', false, false)]
    local procedure MyProcedure(var Rec: Record Customer)
    begin
        if not Confirm(StrSubstNo('¿Desea insertar el cliente %1?', rec."No."), true) then begin
            Error('Proceso Cancelado por el usuario');
        end;
    end;


    [EventSubscriber(ObjectType::Table, Database::TCN_Lines, 'OnAfterVerificarDatosLineasF', '', false, false)]
    local procedure OnAfterVerificarDatosLineasFTableTCNLinesF(var RecLineas: Record TCN_Lines; xRecLineas: Record TCN_Lines)
    begin
        if RecLineas.DescVenta = '' then begin
            RecLineas.DescVenta := 'Sin Descripción';
        end;
        if RecLineas.DescVenta = xRecLineas.DescVenta then begin
            RecLineas.DescVenta := 'Misma Descripción';
        end;
    end;

    // [EventSubscriber(ObjectType::Table, Database::Item, 'OnBeforeValidateEvent', 'Description', false, false)]
    // local procedure OnBeforeValidateEvenDescriptionTableItemF()
    // begin

    // end;
    // [EventSubscriber(ObjectType::Table, Database::Item, 'OnAfterValidateEvent', 'Description', false, false)]
    // local procedure OnBeforeValidateEvenDescriptionTableItemF(var Rec: Record Item)
    // begin
    //     rec.Description := UpperCase(rec.Description);
    // end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::TCN_RegistrosVarios, 'OnFinEjecucionSegundoPlanoF', '', false, false)]
    local procedure OnFinEjecucionSegundoPlanoFTCNRegistrosVariosF(pMomentoFinalizacion: DateTime)
    var
        clMensaje: Label 'Se ha finalizado el proceso en 2º plano el %1';
    begin
        Message(clMensaje, pMomentoFinalizacion);
    end;
}