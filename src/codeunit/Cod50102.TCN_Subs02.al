codeunit 50102 "TCN_Subs02"
{

    SingleInstance = true;
    [EventSubscriber(ObjectType::Codeunit, Codeunit::TCN_RegistrosVarios, 'OnFinEjecucionSegundoPlanoF', '', false, false)]
    local procedure OnFinEjecucionSegundoPlanoCodeunitFTCNRegistrosVariosF(pMomentoFinalizacion: DateTime; pIdSesion: Integer)
    begin
        Message('%1-%2', pIdSesion, pMomentoFinalizacion);
    end;
}