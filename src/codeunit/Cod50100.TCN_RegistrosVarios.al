codeunit 50100 "TCN_RegistrosVarios"
{
    Permissions = tabledata "Sales Invoice Header" = md,
                  tabledata "Sales Invoice Line" = md;
    SingleInstance = true;


    var
        xValor: Text;

    trigger OnRun()

    var
        rlCustomer: Record Customer;
        xlNumCampo: Integer;
        xlCampo: Text;
        xlLinea: Text;
        xlFichero: Text;
        rlTempTempBlobTMP: Record TempBlob temporary;
        xlInStream: InStream;
        xlseleccion: Integer;
        Ventana: Dialog;
        xlNumVeces: Integer;
    begin
        EjemploExcelF();
        // EjemploCalcSumsF();
        // GruposFiltrosF();
        // ExportacionDatos2F();
        exit;
        // ManejoFicherosF();
        // Error('Ejecutando Cu');
        // EjecucionSegundoPlanoF();
        //-------------------------------------------------

        //  Ejemplo Confirm
        // if Confirm('¿Confirma que desea registrar el registro actual?', false) then begin
        //     Message('Registro realizado');
        // end else begin
        //     Error('Ha cancelado el proceso');
        // end;
        //---------------------------------------------------------

        // Ejemplo STRMENU- Menu de seleccion
        // xseleccion := StrMenu('&Enviar, &Facturar, Enviar &y Facturar', 2, '¿Qué desea hacer?');
        // Message(Format(xseleccion));
        //---------------------------------------------------------

        // Ejemplo Ventana dialogo
        // xlNumVeces := 1000000;

        // if GuiAllowed then begin
        //     Ventana.Open('Procesando @1@@@@@@@@@@@@');
        // end;
        // for xlseleccion := 1 to xlNumVeces do begin
        //     if GuiAllowed then begin
        //         Ventana.Update(1, Round(xlseleccion / xlNumVeces * 10000, 1));  //
        //     end;
        // end;
        // if GuiAllowed then begin
        //     Ventana.Close();
        // end;
        //---------------------------------------------------------

        // Ejemplo InStream. es para leer. Subimos un fichero a Business Central

        rlTempTempBlobTMP.Blob.CreateInStream(xlInStream, TextEncoding::Windows);   //TextEncoding para los acentos
        if UploadIntoStream('Seleccione el fichero', '',
                             'Archivos de permitidos(*.txt, *.csv)|*.txt;*.csv | Todos los archivos (*.*)|*.*',
                             xlFichero, xlInStream) then begin
            while not xlInStream.EOS do begin   //lee las lineas del archivo
                xlInStream.ReadText(xlLinea);    //objeto de tipo lista
                xlNumCampo := 0; // el campo es cero, no ha leido ninguno

                rlCustomer.Init();  //inicializo, limpia lo que habia e inicializa los valores
                foreach xlCampo in xlLinea.Split(';') do begin   //divide la linea por ';'
                    // Ejemplo crear Cliente
                    xlNumCampo += 1;
                    case xlNumCampo of
                        1:
                            begin
                                rlCustomer.validate("No.", xlCampo);
                            end;
                        2:

                            rlCustomer.Validate(Name, CopyStr(xlCampo, 1, MaxStrLen(rlCustomer.Name))); //hay que controlar el tamaño

                        3:

                            rlCustomer.Validate(Address, CopyStr(xlCampo, 1, MaxStrLen(rlCustomer.Address))); //hay que controlar el tamaño

                        4:

                            rlCustomer.Validate(City, CopyStr(xlCampo, 1, MaxStrLen(rlCustomer.City))); //hay que controlar el tamaño
                    end;
                end;
                rlCustomer.Insert(true) //inserto los cambios
            end;

        end else begin
            Error('No se pudo subir el fichero');
        end;
    end;




    procedure SetValorF(pValor: Text)
    begin
        xValor := pValor;
        Message('Valor %1 memorizado', xValor);
    end;

    procedure GetValorF(): Text
    begin
        exit(xValor);

    end;

    local procedure RegistrarEntradasF() xSalida: Integer
    begin

    end;

    [TryFunction]
    local procedure EjemploTry()
    begin

    end;

    local procedure EjemploArraysF()
    var
        mlArrayEjemplo: array[100, 200] of Text;
        mlArrayEjemplo2: array[100, 200] of Text;
        i: Integer;
        x: Integer;
    begin
        for i := 1 to ArrayLen(mlArrayEjemplo[1]) do begin
            for x := 1 to ArrayLen(mlArrayEjemplo[2]) do begin
                mlArrayEjemplo[i, x] := 'h';
            end;
        end;
        CopyArray(mlArrayEjemplo2, mlArrayEjemplo, 1);
        //CompressArray(); //comprime y no deja lieneas vacias
    end;

    procedure SobrecargaF(pParametro: BigInteger) xSalida: Text
    begin
        xSalida := Format(pParametro + 1);
    end;

    procedure SobrecargaF(pParametro: Date) xSalida: Text
    begin
        xSalida := Format(CalcDate('+1D', pParametro));
    end;

    procedure SobrecargaF(pParametro: Date; pFormulaFecha: Text) xSalida: Text
    begin
        xSalida := Format(CalcDate(pFormulaFecha, pParametro));
    end;

    procedure EjecucionSegundoPlanoF()
    var
        i: Integer;
        Ventana: Dialog;
        rlAve: Record TCN_Ave;
        clMensaje: Label 'Sesion en 2º plano finalizada';
    begin
        if GuiAllowed then begin
            Ventana.Open('#1############');
        end;
        for i := 1 to 9999999 do begin
            if GuiAllowed then begin
                Ventana.Update(1, i);
            end;
        end;
        // rlAve.Init();
        // rlAve.Validate(Codigo, Format(SessionId()));
        // rlAve.Validate(Nombre, clMensaje);
        // rlAve.Insert(true);

        //Llamamos a la publicacion, ahora creamos la subscripcion
        //OnFinEjecucionSegundoPlanoF(CurrentDateTime, SessionId());
    end;


    local procedure ExportacionDatosF()
    var
        xlFichero: Text;
        rlTempBlobTMP: Record TempBlob temporary;
        xlOutStream: OutStream;
        xlInStream: InStream;
        rlVendor: Record Vendor;
    begin

        xlFichero := 'Proveedores.csv';

        rlTempBlobTMP.Blob.CreateOutStream(xlOutStream);
        rlTempBlobTMP.Blob.CreateInStream(xlInStream);

        with rlVendor do begin
            if FindSet(false) then begin   //false para no modificar datos
                repeat
                    CalcFields("Balance (LCY)");
                    // Caso 1
                    xlOutStream.WriteText("No." + ';');
                    xlOutStream.WriteText(Name + ';');
                    xlOutStream.WriteText(Address + ';');
                    xlOutStream.WriteText(City + ';');
                    xlOutStream.WriteText(format("Balance (LCY)"));
                    xlOutStream.WriteText(); //CR+LF

                    // Caso 1
                    xlOutStream.Writetext(StrSubstNo('%2%1%3%1%4%1%5%1%6',
                                                ';',
                                                "No.",
                                                Name,
                                                Address,
                                                City,
                                                "Balance (LCY)"
                                                ));
                    xlOutStream.WriteText();
                until Next() = 0;
                //como ya tiene registros los quiero descargar
                CopyStream(xlOutStream, xlInStream); //lo paso a InStream
                if DownloadFromStream(xlInStream, 'Fichero a descargar', '',
                                                'Archivos de permitidos(*.txt, *.csv)|*.txt;*.csv | Todos los archivos (*.*)|*.*',
                                                xlFichero) then begin
                    Message('Se ha descargado el Fichero');
                end else begin
                    Error('Se ha producido el siguiente error %1', GetLastErrorText);
                end;
            end;
        end;

    end;


    local procedure ManejoFicherosF()
    var
        rlItem: Record Item;
        rlGLAccount: Record "G/L Account";
        // rlGLAccount2: Record "G/L Account";

        rlTCNA: Record TCN_A;
        xlEnum: Enum TCN_Colores;

        rlSalesLine: Record "Sales Line";
        rlSalesLine2: Record "Sales Line";
        rlSalesShipmentLine: Record "Sales Shipment Line";

    begin
        with rlItem do begin
            SetCurrentKey(Description);
            Ascending(false); //true de menor a mayor

            //filtro de valor a valor. sin caracter especiales
            SetRange("No.", '1000', '2000');    //es un filtro
            SetRange("No.");    //filtro sin parametros

            // lleva caracteres especiales
            SetFilter("No.", '%1..%2|%3..%4', '1000', '1001', '2000', '2020');
            // SetFilter("No.", '%1', cadena);
            // SetFilter("No.", cadena);

            Get();
            Find(); //respeta filtros, se le puede poner parametros (+,<,>,...)
            FindFirst();
            FindLast();
            Next();
            Insert(false);   //false cuando son temporales. 
            DeleteAll();
            ModifyAll("Unit Price", 0, true);    //pide un campo.modifica a 0 el precio y ejecutate


        end;
        // rlGLAccount.SetRange("Account Type", rlGLAccount."Account Type"::Posting);
        // rlTCNA.SetRange(Color, xlEnum::Rojo);

        // rlSalesLine.get(rlsalesLine."Document Type"::Order, '104001', 20000);
        // rlSalesLine."Document Type" := rlSalesLine."Document Type"::Order;
        // rlSalesLine."Document No." := '104001';
        // rlSalesLine."Line No." := 20000;
        // rlSalesLine.Find(); //ejecuta el filtro

        rlSalesLine.SetRange("Document Type", rlSalesLine."Document Type"::Order);
        rlSalesLine.SetRange("Document No.", '104001');
        rlSalesLine.SetFilter("Qty. to Ship", '<>%1', 0);

        if rlSalesLine.FindSet(true) then begin //posiciona el puntero en el primero
            repeat
                // ASI NO FUNCIONA
                // rlSalesLine.Validate("Qty. to Ship", rlSalesLine."Outstanding Quantity");
                // rlSalesLine.Modify(true);

                //SOLUCION 1
                // ponemos todas las claves que tengas
                rlSalesLine2.get(rlSalesLine."Document Type", rlSalesLine."Document No.", rlSalesLine."Line No.");
                rlSalesLine2.Validate("Qty. to Ship", rlSalesLine."Outstanding Quantity");
                rlSalesLine2.Modify(true);

                //SOLUCION 2 
                // No tenemos porque saber las claves
                rlSalesLine2 := rlSalesLine;    //rellenamos los campos
                rlSalesLine2.Find();    //encuentra el registro
                rlSalesLine2.Validate("Qty. to Ship", rlSalesLine."Outstanding Quantity");//asigna el valor
                rlSalesLine2.Modify(true);//modifica

                rlSalesShipmentLine.TransferFields(rlSalesLine);

            until rlSalesLine.Next() = 0;
        end;

        rlGLAccount.LockTable();    //no ponemos nada
        rlGLAccount.TransferFields(rlSalesLine);   //

        // comprueba si tiene permiso y ejecuta una asción o mensaje
        if rlGLAccount.ReadPermission() then begin end;
        if rlGLAccount.WritePermission() then begin end;

        rlGLAccount.AddLink('C:\Users\Alumno06\Documents\AL', 'descripcion del fichero');

    end;

    local procedure GruposFiltrosF()
    var
        rlSalesHeader: Record "Sales Header";
        xlGrupoFiltro: Integer;
    begin
        xlGrupoFiltro := rlSalesHeader.FilterGroup;   //guardo el grupo de filtro
        rlSalesHeader.FilterGroup(xlGrupoFiltro + 2);
        rlSalesHeader.SetRange("Document Type", rlSalesHeader."Document Type"::Order);// filtrar por el valor del option
        rlSalesHeader.SetFilter("Sell-to Customer No.", '<>%1', '');
        rlSalesHeader.FilterGroup(xlGrupoFiltro);
        if page.RunModal(0, rlSalesHeader) in [Action::LookupOK, Action::OK] then begin
            Message(rlSalesHeader."No.");   //que numero de pedido he seleccionado
        end;
    end;

    local procedure GruposFiltros2F()
    var
        rlCustomer: Record Customer;
        plCustomerLookup: Page "Customer Lookup";
    begin
        rlCustomer.FilterGroup(rlCustomer.FilterGroup + 2);
        rlCustomer.SetRange("Salesperson Code", 'JR');
        rlCustomer.FilterGroup(rlCustomer.FilterGroup - 2);  //restauramos el filtro
        rlCustomer.get('27090917');
        plCustomerLookup.SetTableView(rlCustomer);
        plCustomerLookup.SetRecord(rlCustomer);
        plCustomerLookup.LookupMode(true);  //para que salga el boton aceptar
        if plCustomerLookup.RunModal() in [Action::LookupOK, Action::OK] then begin

            plCustomerLookup.GetRecord(rlCustomer);
            //Message(rlCustomer.GetFilters);
            //Message(rlCustomer."No.");
            plCustomerLookup.GetSelectionFilterF(rlCustomer);
            if rlCustomer.FindSet(false) then begin //false porque no voy a modificar
                repeat
                    Message(rlCustomer."No.");
                until rlCustomer.Next() = 0;
            end;
            //Message(rlCustomer.GetFilters);
        end;

    end;

    local procedure EjemploCalcSumsF()
    var
        rlTCN_A: Record TCN_A;
        rlSalesLine: Record "Sales Line";
    begin
        //  CalcFormula = sum ("Sales Line".Amount
        //                         where ("Sell-to Customer No." = field (NumCliente),
        //                                 "Shipment Date" = field (filtroFecha),
        //                                 "No." = field (FiltroProducto)
        //                         )
        //                     );

        //CalcFields 
        /*NAV lo hace internamente y decuelve un calcsums | solo suma una linea, por eso se la pasamos*/
        rlTCN_A.SetRange(NumCliente, '10000');  // seleccionamos el numero cliente 10000
        rlTCN_A.SetFilter(filtroFecha, '%1', dmy2date(20, 1, 2021));
        // rlTCN_A.SetRange(FiltroProducto, 'LS-150');
        if rlTCN_A.FindFirst() then begin
            rlTCN_A.CalcFields(SumaImporte);    //tiene que ser un campo blob
            Message(Format(rlTCN_A.SumaImporte));
        end;

        //Calcsums /* creamos los filtros. Suma todas las lineas*/
        rlSalesLine.SetRange("Sell-to Customer No.", '10000');
        rlSalesLine.SetFilter("Shipment Date", '%1', DMY2Date(20, 1, 2011));
        // rlSalesLine.SetRange("No.", 'LS-150');
        rlSalesLine.CalcSums(Amount);
        Message(Format(rlSalesLine));

    end;

    procedure ImportePedidosFacturasClienteF(prCustomer: Record Customer; prDrillDown: Boolean) xSalida: Decimal
    var
        rlSalesLine: Record "Sales Line";
        rlSalesInvoiceLine: Record "Sales Invoice Line";
        rlSalesLineTMP: Record "Sales Line" temporary;
    begin
        with rlSalesLine do begin
            SetRange("Sell-to Customer No.", prCustomer."No.");
            SetRange("Document Type", "Document Type"::Order);
            if prDrillDown then begin
                if FindSet(false) then begin
                    repeat
                        rlSalesLineTMP.Init();
                        rlSalesLineTMP.TransferFields(rlSalesLine);
                        rlSalesLineTMP.Insert(false);
                    until Next() = 0;
                end;
            end else begin
                CalcSums(Amount);
            end;
            xSalida := Amount;
        end;
        with rlSalesInvoiceLine do begin
            SetRange("Sell-to Customer No.", prCustomer."No.");
            // CalcSums(Amount)
            if prDrillDown then begin
                if FindSet(false) then begin
                    repeat
                        rlSalesLineTMP.Init();
                        rlSalesLineTMP.TransferFields(rlSalesInvoiceLine);
                        rlSalesLineTMP."Document Type" := rlSalesLineTMP."Document Type"::Invoice;
                        rlSalesLineTMP.Insert(false);
                    until Next() = 0;
                end;
            end else begin
                CalcSums(Amount);
            end;
            xSalida += Amount;

        end;
        if prDrillDown then begin
            Page.Run(0, rlSalesLineTMP);    //muestra las lineas de facturas y pedido
        end;
    end;


    procedure NombreClienteF(pCodigo: code[20]) xSalida: Text
    var
        rlCustomer: Record Customer;
    begin
        if rlCustomer.get(pCodigo) then begin
            xSalida := rlCustomer.Name;
        end;
    end;

    local procedure EjemploExcelF()
    var
        xlNombreLibro: Text;
        xlHoja: Text;
        xlInsTream: InStream;
        xlFichero: Text;
        rlTempExcelBuffer: Record "Excel Buffer" temporary;

    begin
        if UploadIntoStream('Seleccione libro Excel',
                            '',
                            ' | Todos los archivos|*.*| Archivos de Excel|*.xls;*.xlsx,',
                            xlFichero,
                            xlInsTream
                            ) then begin
            xlHoja := rlTempExcelBuffer.SelectSheetsNameStream(xlInsTream); //te pide el nombre de la hoja para abrir el libro
            xlNombreLibro := rlTempExcelBuffer.OpenBookStream(xlInsTream, xlHoja);
            // Message(xlNombreLibro);
            rlTempExcelBuffer.ReadSheet();  //lee todas las hojas del libro. mete fila 1 columana 1. Las celdas son registros distintos
            rlTempExcelBuffer.SetFilter("Row No.", '>%1', 1);   //la linea 1 no la lee

            if rlTempExcelBuffer.FindSet() then begin
                repeat
                    Message(StrSubstNo('Fila %1|Columna %2|valor %3',
                                        rlTempExcelBuffer.xlRowID,
                                        rlTempExcelBuffer.xlColID,
                                        rlTempExcelBuffer."Cell Value as Text"
                                        ));

                until rlTempExcelBuffer.Next() = 0;
            end;

        end else begin
            Error('no se ha podido Subir el excel %1', GetLastErrorText);

        end;
    end;

    //---------------------------------------------------------------------------------------






    //Publicacion
    [IntegrationEvent(true, true)]
    local procedure OnFinEjecucionSegundoPlanoF(pMomentoFinalizacion: DateTime; pIdSesion: Integer)
    begin
        //No puede haber código
    end;
}