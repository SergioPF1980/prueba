enum 50100 "TCN_Colores"
{
    Extensible = true;

    value(0; SinColor)
    {
        Caption = ' ';
    }
    value(5; Transparente)
    {

    }
    value(10; Blanco)
    {
        Caption = 'Blanco';
    }
    value(20; Rojo)
    {
        Caption = 'Rojo';
    }
    value(30; Azul)
    {
        Caption = 'Azul';
    }
}